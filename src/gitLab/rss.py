import feedparser
import json

class FeedItem:
    def __init__(self, content) -> None:
        self.updated = content['updated_parsed']
        self.link = content['link'].split("/-/")[0]

class _GetRSS:
    def __init__(self) -> None:
        self.url = "https://gitlab.com/HendrikHeine.atom?feed_token=HYEY9RbUFQebdE-Bv_VG"

    def _fetchRSS(self, url):
        feedItems = []
        feeds = feedparser.parse(url)
        for feed in feeds.entries:
            feedItems.append(FeedItem(feed))
    
        return feedItems
        

class GitLabRSS(_GetRSS):
    def __init__(self) -> None:
        super().__init__()
        self.articles = []

    def fetch(self):
        self.articles = self._fetchRSS(url=self.url)
