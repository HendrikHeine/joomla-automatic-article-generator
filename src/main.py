import os
import datetime
import time
from time import sleep
from gitLab.rss import GitLabRSS
from gitLab.rss import FeedItem
from joomla import dateTime

rss = GitLabRSS()
rss.fetch()

def writeLatest(latest):
    with open(file="latest.txt", mode="w") as file:
        file.write(str(latest))
        file.close()

def readLatest():
    with open(file="latest.txt", mode="r") as file:
        latest = file.read()
        file.close()
    return latest

latestTime = float(readLatest())

while True:
    try:
        print(dateTime.get())
        for article in rss.articles:
            article:FeedItem
            unix = datetime.datetime(
                year=article.updated[0],
                month=article.updated[1],
                day=article.updated[2],
                hour=article.updated[3],
                minute=article.updated[4]
            )
            print(unix)
            #print(f"Updated: {article.updated[2]}.{article.updated[1]}.{article.updated[0]} at {article.updated[3]}:{article.updated[4]}")
            #print(article.link)
            if latestTime < time.mktime(unix.timetuple()):
                latestTime = time.mktime(unix.timetuple())
                writeLatest(latestTime)
                print("Updated latest")
            else:
                print("Nothing changed")
        print("------")
        sleep(20)
        os.system("clear")
    except:
        raise Exception